# README #
This is the FPGA project for the Project Pluto Board.
The Project Pluto Board is a board that has an STM32F4 and a Spartan 6 connected
together over the STM32F4's FSMC(Flexible Static Memory Controller) bus
on this board the STM32F4 is running a version of the NuttX OS.

The NuttX repo is [here](https://bitbucket.org/nuttx/)
and the modified version that the Project Pluto Board has is [here](https://github.com/vxmdesign/yartos)

The purpose of the FPGA is to handle most of the IO so the processor can be doing cooler things
and when it needs I/O it can just ask/grabbed what it needs from the fpga since the FSMC bus 
lets the STM32 have a pointers to the FPGA's memory.

### How do I get set up? ###

You will need to download the ISE toolchain from Xilinx
[here](http://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/design-tools.html)

Start a new project and set the Design Properties as:

* Family: Spartan 6
* Device: XC6SLX16
* Pakcage: FTG256
* Speed: -2

And the pins are specific to the Project Pluto board. If you have a different board you will need to change the top_pins.ucf file to match your board.

This project also assumes(important for all clock dividers)
that you are starting the STM32 FSMC bus at 90MHz with a data latch of 3 clock cycles

### Deployment instructions
Assuming you have the prj pluto board and the modified Nuttx running on the STM32.

Then when you have an fpga image(*.bin file) from the Xilinx toolchain you want to put on the board
name it top.bin, put it on a micro sd card, and plug it into the microsdcard holder on the board.
Power on the board and when the shell comes up:

On the nuttx shell type these cmds

* nsh> mount -t vfat /dev/mmcsd0 /mnt/sdcard
* nsh> fpga_prg //programmes the fpga with a file named top.bin on the sdcard
* nsh> startfsmc 2 //starts the 90MHz clk for the FSMC bus
* nsh> dl 3 //sets data latch for the FSMC bus to 3 clock cycles

Now your fpga is programmed, you may want to write some nuttx applications to interact with
fpga's memory.

# Support peripherals #
currently on the master branch:

- 3 fixed Frequency PWM generators
- FSMC Bus for communciation with the STM32
- BLE (Bluefruit LE - Bluetooth Low Energy (BLE 4.0) - nRF8001 Breakout - v1.0)

currently working on:

- Xbee
- LED Array
- DAC
- Frequency Generator 

# Memory map $
The current Memory Map for the STM32 looking to read and/or write to the FPGA's internal registers are

Address offset |  Register | R/W
-------------- | --------- | ---
0 - 63 | Test RAM | R/W
98 | interrupt 0 enable | R/W
99 | debug register 1 | R
199 | 0xbeef | R
200 | test register | R/W
256-287 | BLE command data | R/W
288 | BLE send command trigger | R/W
289 | BLE enable | R/W
290 | BLE reset register | R/W
291 | interrupt 0 clear | R/W
4096-4128 | BLE response data | R

if you try to read any other address on the data bus it should read back as 0xdead

With the modified version of nuttx you can read and write to addresses on this bus from the command line:

fr (add) //to read

fw (add) (data) //to write

### Who am I? ###
Hey my name is Elena Pliakas, I am an Embedded Systems Engineer.
I enjoy using FPGAs and on embedded systems! Contact me here on bitbucket :)