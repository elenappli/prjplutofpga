----------------------------------------------------------------------------------
-- Company: Thirdshift
-- Engineer: Elena Pliakas
-- 
-- Create Date:    19:17:25 03/31/2015 
-- Design Name: 
-- Module Name:    top - Behavioral 
-- Project Name: Prj Pluto
-- Target Devices: Spartan 6
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
--use UNISIM.VComponents.all;

entity top is
	generic(
		depth : integer := 63;
		width : integer := 16
	);

	port(
		gls_rst    : in    std_logic;
		led0       : out   std_logic;
		led1       : out   std_logic;
		led2       : out   std_logic;
		led3       : out   std_logic;
		fsmc_clk   : in    std_logic;
		A25_16     : in    std_logic_vector(9 downto 0);
		AD15_0     : inout std_logic_vector(15 downto 0);
		NE1        : in    std_logic;
		NE2        : in    std_logic;
		NOE        : in    std_logic;
		NWE        : in    std_logic;
		NADV       : in    std_logic;
		NWAIT      : out   std_logic;
		NBL1       : in    std_logic;
		NBL0       : in    std_logic;
		fpga_p0    : out   std_logic;
		fpga_p1    : out   std_logic;
		fpga_p2    : out   std_logic;
		fpga_p3    : in    std_logic;
		fpga_p4    : out   std_logic;
		fpga_p5    : out   std_logic;
		fpga_p6    : out   std_logic;
		fpga_p7    : out   std_logic;
		fpga_p8    : out   std_logic;
		fpga_p9    : in    std_logic;   --ble_rdy
		fpga_p10   : in    std_logic;
		fpga_p11   : in    std_logic;   --ble_act
		fpga_p13   : out   std_logic;   --ble_reset
		fpga_p15   : out   std_logic;
		fpga_intr0 : out   std_logic
	);
end top;

architecture Behavioral of top is
	-------------------------
	-- declare components  --
	-------------------------
	-------------------------
	-- Signals declaration --
	-------------------------

	-------------------------
	-- 1 Hz LED -------------
	-------------------------
	signal led0_enable : std_logic                     := '0';
	signal led0_reg    : std_logic                     := '0';
	signal led0_cc     : std_logic_vector(25 downto 0) := "00000000000000000000000000";
	signal led0_count  : std_logic_vector(25 downto 0) := "10101011101010010101000000";
	-- ----- --
	-- usec  --
	-- ----- --	
	signal usecond     : std_logic                     := '0';
	signal ucounter    : std_logic_vector(7 downto 0)  := "00000000";
	signal ucount      : std_logic_vector(7 downto 0)  := "01011010"; --startfsmc 2
	-------------------------
	-- PWM generators -------
	-------------------------
	signal pwm1_t_on   : std_logic_vector(15 downto 0) := "0000000000000000";
	signal pwm1_cc     : std_logic_vector(15 downto 0) := "0000000000000000";
	signal pwm2_t_on   : std_logic_vector(15 downto 0) := "0000000000000000";
	signal pwm2_cc     : std_logic_vector(15 downto 0) := "0000000000000000";
	signal pwm3_t_on   : std_logic_vector(15 downto 0) := "0000000000000000";
	signal pwm3_cc     : std_logic_vector(15 downto 0) := "0000000000000000";
	signal pwm1_reg    : std_logic                     := '0';
	signal pwm2_reg    : std_logic                     := '0';
	signal pwm3_reg    : std_logic                     := '0';

	signal freq_gen1 : std_logic                     := '0';
	signal ton_ctr   : std_logic_vector(15 downto 0) := "0000000000000000";
	signal ton_cnt   : std_logic_vector(15 downto 0) := "0000000000000000";
	signal toff_ctr  : std_logic_vector(15 downto 0) := "0000000000000000";
	signal toff_cnt  : std_logic_vector(15 downto 0) := "0000000000000000";
	signal clk_cnt   : std_logic_vector(15 downto 0) := "0000000000100000";
	signal clk_ctr   : std_logic_vector(15 downto 0) := "0000000000000000";
	-------------------------
	-- FSMC Mem Bus ---------
	-------------------------
	signal AD15_0out : std_logic_vector(15 downto 0) := "0000000000000000";
	signal add       : std_logic_vector(25 downto 0) := "00000000000000000000000000";
	signal radd      : std_logic_vector(25 downto 0) := "00000000000000000000000000";
	signal ndata     : std_logic_vector(15 downto 0) := "0000000000000000";

	type bus_state is (reset, wrt, rd);
	signal fsmc_bus_state : bus_state;

	type write_states is (dwait, datal, data);
	signal write_state : write_states;

	signal dl_counter : std_logic_vector(1 downto 0) := "00";
	signal dl_count   : std_logic_vector(1 downto 0) := "10"; --dl 3

	-------------------------
	-- Test RAM -------------
	-------------------------
	type ram_type is array (0 to depth - 1) of std_logic_vector(width - 1 downto 0);
	signal tmp_ram : ram_type;          --RAM 
	signal ram_mux : std_logic_vector(7 downto 0) := "00000000";

	-------------------------
	-- Debug Registers ------
	-------------------------
	signal test_reg   : std_logic_vector(15 downto 0) := "0000000000000000";
	signal test_reg1  : std_logic_vector(15 downto 0) := "0000000000000000";
	signal test_reg2  : std_logic_vector(15 downto 0) := "0000000000000000";
	signal test_reg3  : std_logic_vector(15 downto 0) := "0000000000000000";
	signal test_reg4  : std_logic_vector(15 downto 0) := "0000000000000000";
	signal test_reg5  : std_logic_vector(15 downto 0) := "0000000000000000";
	signal debug_reg1 : std_logic_vector(15 downto 0) := "0000000000000000";
	signal debug_reg2 : std_logic_vector(15 downto 0) := "0000000000000000";
	signal debug_reg3 : std_logic_vector(15 downto 0) := "0000000000000000";
	signal debug_reg4 : std_logic_vector(15 downto 0) := "0000000000000000";
	signal debug_reg5 : std_logic_vector(15 downto 0) := "0000000000000000";

	-- ----- --
	-- ble   --
	-- ----- --	
	type ble_array_t is array (0 to 31) of std_logic_vector(7 downto 0);
	signal ble_array    : ble_array_t;
	signal ble_array_rd : ble_array_t;
	type ble_states is (idle, wrt, rd, rd_pw);
	signal ble_st : ble_states;

	signal ble_clk_enable : std_logic                    := '0';
	signal ble_sck_ct     : std_logic_vector(5 downto 0) := "100000"; --startfsmc 2 
	signal ble_sck_cc     : std_logic_vector(5 downto 0) := "000000";
	signal ble_sck        : std_logic                    := '0';
	signal ble_sck_fe     : std_logic                    := '0';
	signal ble_status     : std_logic_vector(7 downto 0) := "00000000";
	signal ble_bit_cc     : std_logic_vector(7 downto 0) := "00000000";
	signal ble_wbyte      : std_logic_vector(7 downto 0) := "00000000";
	signal ble_byte_cc    : std_logic_vector(7 downto 0) := "00000000";
	signal ble_rbyte      : std_logic_vector(7 downto 0) := "00000000";
	signal ble_rbyte_cc   : std_logic_vector(7 downto 0) := "00000000";
	signal ble_rbyte_cnt  : std_logic_vector(7 downto 0) := "00000000";
	signal intr0          : std_logic                    := '0';
	signal intr0_en       : std_logic_vector(7 downto 0) := "00000000";
	signal intr0_clear    : std_logic                    := '1';
	signal ble_enable     : std_logic                    := '0';
	signal ble_go_hold    : std_logic                    := '0';
	signal ble_req        : std_logic                    := '1';
	signal ble_rdy        : std_logic                    := '0';
	signal ble_go         : std_logic                    := '0';
	signal ble_byte_cnt   : std_logic_vector(7 downto 0) := "00000000";
	signal ble_miso       : std_logic                    := '0';
	signal ble_act        : std_logic                    := '0';
	signal ble_rst_reg    : std_logic_vector(7 downto 0) := "11111111";

	-- ----- --
	-- dac   --
	-- ----- --	
	signal dac_clk_enable : std_logic                     := '0';
	signal dac_clk        : std_logic                     := '0';
	signal dac_clk_cnt    : std_logic_vector(7 downto 0)  := "00101101"; --startfsmc 2
	signal dac_clk_cc     : std_logic_vector(7 downto 0)  := "00000000";
	signal dac_clk_edg    : std_logic                     := '0';
	signal dac_flag       : std_logic                     := '0';
	signal dac_sela       : std_logic_vector(2 downto 0)  := "000";
	signal dac_rng        : std_logic                     := '0';
	signal dac_data       : std_logic_vector(7 downto 0)  := "00000000";
	signal dac_reg        : std_logic_vector(12 downto 0) := "0000000000000";
	signal dac_load       : std_logic                     := '1';

	signal dac_ldac     : std_logic                    := '0';
	signal dac_proc     : std_logic                    := '0';
	signal dac_bit_ctr  : std_logic_vector(3 downto 0) := "0000";
	signal dac_bit_cnt  : std_logic_vector(3 downto 0) := "1101"; -----------
	signal dac_load_cnt : std_logic_vector(7 downto 0) := "00011001"; --25
	signal dac_ldac_cnt : std_logic_vector(7 downto 0) := "00100010"; --50
	signal dac_load_ctr : std_logic_vector(7 downto 0) := "00000000";

	-- ----- --
	-- Fan   --
	-- ----- --	
	signal fan         : std_logic_vector(4 downto 0) := "00000";
	signal fan_in      : std_logic                    := '0';
	signal fan_trigger : std_logic                    := '0';

	-- ----- --
	-- LEDA  --
	-- ----- --	
	--constants
	signal led_array_clk_T0H : std_logic_vector(6 downto 0) := "0100100"; --0.4us
	signal led_array_clk_T0L : std_logic_vector(6 downto 0) := "1001100"; --0.85us

	--
	--90MHz->1.25Mhz
	--count = 36 *2
	signal led_array_clk_T1H : std_logic_vector(6 downto 0)  := "1001000"; --0.8us
	signal led_array_clk_T1L : std_logic_vector(6 downto 0)  := "0101000"; --0.45us
	signal led_array_lcount  : std_logic_vector(12 downto 0) := "1000110010100"; --50us
	--trigger
	signal led_array_go      : std_logic                     := '0';
	--ram
	type led_ram_type is array (0 to 89) of std_logic_vector(7 downto 0);
	signal led_ram : led_ram_type;
	--states
	type led_array_states is (reset, ton, toff, latch);
	signal led_array_state : led_array_states;

	--counters
	signal led_array_clk_ton_ctr  : std_logic_vector(6 downto 0)  := "0000000";
	signal led_array_clk_toff_ctr : std_logic_vector(6 downto 0)  := "0000000";
	signal led_array_lcounter     : std_logic_vector(12 downto 0) := "0000000000000";

	signal led_array_ram_index_sel : std_logic_vector(7 downto 0) := "00000000";
	signal led_array_data_bit_sel  : std_logic_vector(7 downto 0) := "00000000";

	--status
	signal led_array_clk_ton  : std_logic_vector(6 downto 0) := "0000000";
	signal led_array_clk_toff : std_logic_vector(6 downto 0) := "0000000";

	signal led_array_data : std_logic_vector(7 downto 0) := "00000000";
	signal led_array_dout : std_logic                    := '0';

begin                                   -- architecture body
	-------------------------
	-- declare instance -----
	-------------------------

	--------------------------
	-- process blocks --------
	--------------------------  

	clk_divider : process(fsmc_clk)
	begin
		if rising_edge(fsmc_clk) then
			if (led0_cc = led0_count) then
				led0_reg <= not led0_reg;
				led0_cc  <= (others => '0');
			else
				led0_reg <= led0_reg;
				led0_cc  <= led0_cc + '1';
			end if;
		end if;
	end process;

	----------------------------------------------------------------------------------
	----------------------------------------------------------------------------------	
	----------------------------------------------------------------------------------	
	microsec : process(fsmc_clk)
	begin
		if rising_edge(fsmc_clk) then
			if (ucounter <= ucount) then
				ucounter <= ucounter + '1';
				usecond  <= '0';
			elsif (ucounter > ucount) then
				ucounter <= (others => '0');
				usecond  <= '1';
			end if;
		end if;
	end process;

	----------------------------------------------------------------------------------
	----------------------------------------------------------------------------------	
	----------------------------------------------------------------------------------	
	pwm1 : process(fsmc_clk)
	begin
		if rising_edge(fsmc_clk) then
			pwm1_cc <= pwm1_cc + '1';
			if (pwm1_cc <= pwm1_t_on) then
				pwm1_reg <= '0';
			else
				pwm1_reg <= '1';
			end if;
		end if;
	end process;

	pwm2 : process(fsmc_clk)
	begin
		if rising_edge(fsmc_clk) then
			pwm2_cc <= pwm2_cc + '1';
			if (pwm2_cc <= pwm2_t_on) then
				pwm2_reg <= '0';
			else
				pwm2_reg <= '1';
			end if;
		end if;
	end process;

	pwm3 : process(fsmc_clk)
	begin
		if rising_edge(fsmc_clk) then
			pwm3_cc <= pwm3_cc + '1';
			if (pwm3_cc <= pwm3_t_on) then
				pwm3_reg <= '0';
			else
				pwm3_reg <= '1';
			end if;
		end if;
	end process;
	--------------------------------------------------------------------------------
	--------------------------------------------------------------------------------	
	--------------------------------------------------------------------------------
	freq_generator1 : process(fsmc_clk)
	begin
		if rising_edge(fsmc_clk) then
			if (fan(4 downto 3) = "01") then
				fan_trigger <= '1';
			elsif ((fan(4 downto 3) = "11") and (fan_trigger = '1')) then
				if (usecond = '1') then
					if (toff_ctr < toff_cnt) then --count off
						toff_ctr    <= toff_ctr + '1';
						freq_gen1   <= '0';
						fan_trigger <= '1';
					elsif (ton_ctr < ton_cnt) then --count on
						ton_ctr     <= ton_ctr + '1';
						freq_gen1   <= '1';
						fan_trigger <= '1';
					else
						ton_ctr     <= (others => '0');
						toff_ctr    <= (others => '0');
						freq_gen1   <= '0';
						fan_trigger <= '0';
					end if;
				end if;
			else
				freq_gen1   <= '0';
				ton_ctr     <= (others => '0');
				toff_ctr    <= (others => '0');
				fan_trigger <= '0';
			end if;
		end if;
	end process;
	--------------------------------------------------------------------------------
	--------------------------------------------------------------------------------	
	--------------------------------------------------------------------------------
	fan_meta : process(fsmc_clk)
	begin
		if rising_edge(fsmc_clk) then
			fan <= fan(3 downto 0) & fan_in;
		end if;
	end process;
	--------------------------------------------------------------------------------	
	--------------------------------------------------------------------------------
	ble_clk : process(fsmc_clk)
	begin
		if rising_edge(fsmc_clk) then
			if (ble_clk_enable = '1') then
				--debug_reg5 <= debug_reg5 + '1';
				if (ble_sck_cc = ble_sck_ct) then
					ble_sck_cc <= (others => '0');
					ble_sck    <= not ble_sck;
					if (ble_sck = '1') then
						ble_sck_fe <= '1';
					end if;
				else
					ble_sck_cc <= ble_sck_cc + '1';
					ble_sck    <= ble_sck;
					ble_sck_fe <= '0';
				end if;
			else
				ble_sck_cc <= (others => '0');
				ble_sck    <= '0';
				ble_sck_fe <= '0';
			end if;
		end if;
	end process;
	---------------------------------------------------
	---------------------------------------------------
	---------------------------------------------------
	ble : process(fsmc_clk)
	begin
		if rising_edge(fsmc_clk) then
			case (ble_st) is
				when idle =>
					ble_status     <= "00000001";
					ble_clk_enable <= '0';
					ble_bit_cc     <= "00001000";
					ble_wbyte      <= "00000000";
					ble_byte_cc    <= (others => '0');
					ble_rbyte      <= (others => '0');
					ble_rbyte_cnt  <= "00000010";
					ble_rbyte_cc   <= (others => '0');
					intr0          <= '0';
					ble_req        <= '1';

					if (ble_go = '1') then
						ble_go_hold <= '1';
					else
						ble_go_hold <= ble_go_hold;
					end if;

					if (ble_enable = '0') then
						ble_st       <= idle;
						ble_byte_cnt <= ble_byte_cnt;
						ble_wbyte    <= ble_wbyte;
					elsif ((ble_rdy = '0')) then --read
						ble_st       <= rd;
						ble_byte_cnt <= ble_byte_cnt;
						ble_wbyte    <= ble_wbyte;
					elsif (ble_go_hold = '1') then --write
						ble_st       <= wrt;
						ble_byte_cnt <= ble_array(0) + 1;
						ble_wbyte    <= ble_array(0);
					else
						ble_st       <= idle;
						ble_byte_cnt <= ble_byte_cnt;
						ble_wbyte    <= ble_wbyte;
					end if;

				when wrt =>
					ble_status    <= "00000010";
					intr0         <= '0';
					ble_go_hold   <= '0';
					ble_rbyte     <= ble_rbyte;
					ble_rbyte_cnt <= ble_rbyte_cnt;
					ble_rbyte_cc  <= ble_rbyte_cc;
					ble_byte_cnt  <= ble_byte_cnt;

					if (ble_byte_cc < ble_byte_cnt) then
						ble_req <= '0';
						ble_st  <= ble_st;
						if (ble_rdy = '0') then
							if (ble_bit_cc > "00000000") then
								ble_clk_enable <= '1';
								if (ble_sck_fe = '1') then --clk edge
									ble_wbyte  <= '0' & ble_wbyte(7 downto 1);
									ble_bit_cc <= ble_bit_cc - '1';
								else
									ble_wbyte  <= ble_wbyte;
									ble_bit_cc <= ble_bit_cc;
								end if;
							else
								ble_clk_enable <= '0';
								ble_wbyte      <= ble_array(conv_integer(ble_byte_cc + 1));
								ble_bit_cc     <= "00001000";
								ble_byte_cc    <= ble_byte_cc + '1';
							end if;
						else
							ble_clk_enable <= ble_clk_enable;
							ble_wbyte      <= ble_wbyte;
							ble_bit_cc     <= ble_bit_cc;
							ble_byte_cc    <= ble_byte_cc;
						end if;
					else
						ble_req <= '1';
						if (ble_rdy = '0') then
							ble_st <= wrt;
						else
							ble_st <= idle;
						end if;
					end if;

				when rd =>              --debug_reg1 <= debug_reg1 + '1';
					ble_status   <= "00000011";
					ble_wbyte    <= ble_wbyte;
					ble_byte_cc  <= ble_byte_cc;
					ble_byte_cnt <= ble_byte_cnt;
					ble_req      <= '0';
					if (ble_rbyte_cc < ble_rbyte_cnt) then
						ble_st <= ble_st;
						if (ble_bit_cc > "00000000") then
							ble_clk_enable <= '1';
							if (ble_sck_fe = '1') then --rising edge
								ble_wbyte  <= "00000000";
								ble_rbyte  <= ble_miso & ble_rbyte(7 downto 1);
								ble_bit_cc <= ble_bit_cc - '1';
							else
								ble_wbyte  <= ble_wbyte;
								ble_rbyte  <= ble_rbyte;
								ble_bit_cc <= ble_bit_cc;
							end if;
						else
							ble_clk_enable <= '0';
							ble_wbyte      <= ble_wbyte;
							ble_rbyte      <= ble_rbyte;
							ble_bit_cc     <= "00001000";

							ble_array_rd(conv_integer(ble_rbyte_cc)) <= ble_rbyte;
							ble_rbyte_cc                             <= ble_rbyte_cc + '1';
							if (ble_rbyte_cc = "00000001") then
								ble_rbyte_cnt <= ble_rbyte_cnt + ble_rbyte;
							else
								ble_rbyte_cnt <= ble_rbyte_cnt;
							end if;
						end if;
					else
						intr0      <= '1';
						debug_reg1 <= debug_reg1 + '1';
						ble_req    <= '1';
						if (ble_rdy = '1') then
							ble_st <= idle;
						else
							ble_st <= rd;
						end if;

					end if;
				when others => ble_st <= idle;

			end case;
		end if;
	end process;

	----------------------------------------------------------------------------------
	----------------------------------------------------------------------------------	
	----------------------------------------------------------------------------------
	dac_sck : process(fsmc_clk)
	begin
		if rising_edge(fsmc_clk) then
			if (dac_clk_enable = '1') then
				if (dac_clk_cc = dac_clk_cnt) then
					dac_clk_cc <= (others => '0');
					dac_clk    <= not dac_clk;
					if (dac_clk = '1') then -- change data on rising edge
						dac_clk_edg <= '1';
					end if;
				else
					dac_clk_cc  <= dac_clk_cc + '1';
					dac_clk     <= dac_clk;
					dac_clk_edg <= '0';
				end if;
			else
				dac_clk_cc  <= (others => '0');
				dac_clk     <= '0';
				dac_clk_edg <= '0';
			end if;
		end if;
	end process;

	dac_shiftreg : process(fsmc_clk)
	begin
		if rising_edge(fsmc_clk) then
			if (dac_flag = '1') then
				dac_clk_enable <= '0';
				dac_load       <= '1';
				dac_ldac       <= '0';
				dac_proc       <= '1';

				dac_reg <= "0" & dac_sela & dac_rng & dac_data;

				dac_bit_ctr  <= (others => '0');
				dac_load_ctr <= (others => '0');

			elsif (dac_proc = '1') then
				if (dac_bit_ctr < dac_bit_cnt) then
					dac_clk_enable <= '1';
					dac_load       <= '1';
					dac_ldac       <= '0';
					dac_proc       <= '1';

					if (dac_clk_edg = '1') then
						dac_reg     <= dac_reg(11 downto 0) & '0';
						dac_bit_ctr <= dac_bit_ctr + '1';
					end if;
				else
					dac_clk_enable <= '0';

					if (dac_load_ctr < dac_load_cnt) then
						dac_load     <= '0';
						dac_ldac     <= '0';
						dac_proc     <= '1';
						dac_load_ctr <= dac_load_ctr + '1';
					elsif (dac_load_ctr < dac_ldac_cnt) then
						dac_load     <= '1';
						dac_ldac     <= '0';
						dac_proc     <= '1';
						dac_load_ctr <= dac_load_ctr + '1';
					else
						dac_load <= '1';
						dac_ldac <= '0';
						dac_proc <= '0';
					end if;
				end if;
			else
				dac_clk_enable <= '0';
				dac_load       <= '1';
				dac_ldac       <= '0';
				dac_reg        <= (others => '0');
			end if;
		end if;
	end process;

	----------------------------------------------------------------------------------
	----------------------------------------------------------------------------------	
	----------------------------------------------------------------------------------
	led_array : process(fsmc_clk)
	begin
		if rising_edge(fsmc_clk) then
			case (led_array_state) is
				when reset => led_array_dout <= '0';

					led_array_ram_index_sel <= (others => '0');
					led_array_data_bit_sel  <= (others => '0');

					led_array_clk_ton_ctr  <= (others => '0');
					led_array_clk_toff_ctr <= (others => '0');
					led_array_lcounter     <= (others => '0');
					--next state logic
					if (led_array_go = '1') then
						led_array_state <= ton;
						led_array_data  <= led_ram(0);
						--debug_reg2 <= debug_reg2 + '1';
						debug_reg2      <= "00000000" & led_ram(0);
						if (led_ram(0)(7) = '1') then
							led_array_clk_ton  <= led_array_clk_T1H;
							led_array_clk_toff <= led_array_clk_T1L;
						else
							led_array_clk_ton  <= led_array_clk_T0H;
							led_array_clk_toff <= led_array_clk_T0L;
						end if;

					else
						led_array_state <= reset;
					end if;

				when ton => led_array_dout <= '1';

					led_array_clk_toff_ctr <= (others => '0');
					led_array_data_bit_sel <= led_array_data_bit_sel;

					--next state logic
					if (led_array_clk_ton_ctr = led_array_clk_ton) then
						led_array_state       <= toff;
						debug_reg3            <= debug_reg3 + '1';
						led_array_clk_ton_ctr <= (others => '0');

						if (led_array_data_bit_sel = 7) then
							led_array_ram_index_sel <= led_array_ram_index_sel + '1';
							led_array_data_bit_sel  <= (others => '0');
							led_array_data          <= led_array_data(6 downto 0) & '0';
						else
							led_array_data         <= led_ram(conv_integer(led_array_ram_index_sel));
							led_array_data_bit_sel <= led_array_data_bit_sel + '1';
						end if;

					else
						led_array_state       <= ton;
						led_array_clk_ton_ctr <= led_array_clk_ton_ctr + '1';
					end if;

				when toff => led_array_dout <= '0';

					led_array_clk_ton_ctr   <= (others => '0');
					led_array_ram_index_sel <= led_array_ram_index_sel;
					--next state logic
					if (led_array_ram_index_sel = 90) then -- 89
						led_array_state <= latch; --reset;
					elsif (led_array_clk_toff_ctr = led_array_clk_toff) then
						debug_reg4             <= debug_reg4 + '1';
						led_array_state        <= ton;
						led_array_clk_toff_ctr <= (others => '0');

						if (led_array_data(7) = '1') then
							led_array_clk_ton  <= led_array_clk_T1H;
							led_array_clk_toff <= led_array_clk_T1L;
						else
							led_array_clk_ton  <= led_array_clk_T0H;
							led_array_clk_toff <= led_array_clk_T0L;
						end if;

					else
						led_array_state        <= toff;
						led_array_clk_toff_ctr <= led_array_clk_toff_ctr + '1';
					end if;
				when latch => led_array_dout <= '0';
					if (led_array_lcounter < led_array_lcount) then
						led_array_state    <= latch;
						led_array_lcounter <= led_array_lcounter + '1';
					else
						led_array_state    <= reset;
						led_array_lcounter <= (others => '0');
					--debug_reg5         <= debug_reg5 + '1';
					end if;

				when others => led_array_state <= reset;
					led_array_dout <= '0';
			end case;
		end if;
	end process;
	----------------------------------------------------------------------------
	----------------------------------------------------------------------------
	----------------------------------------------------------------------------
	fsmc_write : process(fsmc_clk)
	begin
		if rising_edge(fsmc_clk) then
			case (fsmc_bus_state) is
				when reset =>
					ble_go       <= '0';
					dac_flag     <= '0';
					led_array_go <= '0';
					if ((NE1 = '0') and (NADV = '0') and (NWE = '0')) then
						fsmc_bus_state <= wrt;
						write_state    <= dwait;
						add            <= A25_16 & AD15_0;
					elsif ((NE1 = '0') and (NADV = '0') and (NWE = '1')) then
						fsmc_bus_state <= rd;
						radd           <= A25_16 & AD15_0;
					else
						fsmc_bus_state <= reset;
					end if;
				when wrt =>
					case (write_state) is
						when dwait =>
							ble_go         <= '0';
							dac_flag       <= '0';
							led_array_go   <= '0';
							fsmc_bus_state <= wrt;
							if (NADV = '1') then
								if (dl_counter < dl_count) then
									dl_counter  <= dl_counter + '1';
									write_state <= dwait;
								else
									dl_counter  <= (others => '0');
									write_state <= datal;
								end if;
							else
								dl_counter  <= (others => '0');
								write_state <= dwait;
							end if;
						when datal =>
							ndata       <= AD15_0;
							write_state <= data;
						when data =>
							case (conv_integer(add)) is
								when 0 to depth =>
									tmp_ram(conv_integer(add)) <= ndata;
									--flags
									ble_go                     <= '0';
									dac_flag                   <= '0';
									led_array_go               <= '0';
								when 98 =>
									intr0_en     <= ndata(7 downto 0);
									--flags
									ble_go       <= '0';
									dac_flag     <= '0';
									led_array_go <= '0';
								when 134 =>
									pwm1_t_on    <= ndata;
									--flags
									ble_go       <= '0';
									dac_flag     <= '0';
									led_array_go <= '0';
								when 135 =>
									pwm2_t_on    <= ndata;
									--flags
									ble_go       <= '0';
									dac_flag     <= '0';
									led_array_go <= '0';
								when 136 =>
									pwm3_t_on    <= ndata;
									--flags
									ble_go       <= '0';
									dac_flag     <= '0';
									led_array_go <= '0';
								when 160 =>
									ton_cnt      <= ndata;
									--flags
									ble_go       <= '0';
									dac_flag     <= '0';
									led_array_go <= '0';
								when 161 =>
									toff_cnt     <= ndata;
									--flags
									ble_go       <= '0';
									dac_flag     <= '0';
									led_array_go <= '0';
								when 197 =>
									led0_enable  <= ndata(0);
									--flags
									ble_go       <= '0';
									dac_flag     <= '0';
									led_array_go <= '0';
								when 198 =>
									test_reg     <= ndata;
									--flags
									ble_go       <= '0';
									dac_flag     <= '0';
									led_array_go <= '0';
								when 200 =>
									dac_flag     <= '1';
									--flags
									ble_go       <= '0';
									led_array_go <= '0';
								when 201 =>
									dac_sela     <= ndata(2 downto 0);
									--flags
									ble_go       <= '0';
									dac_flag     <= '0';
									led_array_go <= '0';
								when 202 =>
									dac_rng      <= ndata(0);
									--flags
									ble_go       <= '0';
									dac_flag     <= '0';
									led_array_go <= '0';
								when 203 =>
									dac_data     <= ndata(7 downto 0);
									--flags
									ble_go       <= '0';
									dac_flag     <= '0';
									led_array_go <= '0';
								when 256 to 287 =>
									ble_array(conv_integer(add(4 downto 0))) <= ndata(7 downto 0);
									--if (add = 273) then
									--	debug_reg5 <= ndata;
									--end if;
									--flags
									ble_go       <= '0';
									dac_flag     <= '0';
									led_array_go <= '0';
								when 288 =>
									ble_go       <= '1';
									--flags
									dac_flag     <= '0';
									led_array_go <= '0';
								when 289 =>
									ble_enable   <= ndata(0);
									--flags
									ble_go       <= '0';
									dac_flag     <= '0';
									led_array_go <= '0';
								when 290 =>
									ble_rst_reg  <= ndata(7 downto 0);
									--flags
									ble_go       <= '0';
									dac_flag     <= '0';
									led_array_go <= '0';
								when 291 =>
									intr0_clear  <= ndata(0);
									--flags
									ble_go       <= '0';
									dac_flag     <= '0';
									led_array_go <= '0';
								when 1023 =>
									led_array_go <= '1';
									--flags
									ble_go       <= '0';
									dac_flag     <= '0';
								when 1024 to 1113 =>
									led_ram(conv_integer(add(7 downto 0))) <= ndata(7 downto 0);
									--flags
									ble_go                                 <= '0';
									dac_flag                               <= '0';
									led_array_go                           <= '0';
								when others =>
									test_reg1    <= "0000000000000000";
									--flags
									ble_go       <= '0';
									dac_flag     <= '0';
									led_array_go <= '0';
							end case;
							fsmc_bus_state <= reset;
							write_state    <= dwait;
					end case;
				when rd =>
					ble_go       <= '0';
					dac_flag     <= '0';
					led_array_go <= '0';
					case (conv_integer(radd)) is
						when 0 to depth =>
							AD15_0out <= tmp_ram(conv_integer(radd(5 downto 0)));
						when 98 =>
							AD15_0out <= "00000000" & intr0_en;
						when 99 =>
							AD15_0out <= debug_reg1;
						when 100 =>
							AD15_0out <= debug_reg2;
						when 101 =>
							AD15_0out <= debug_reg3;
						when 102 =>
							AD15_0out <= debug_reg4;
						when 103 =>
							AD15_0out <= debug_reg5;
						when 104 =>
							AD15_0out <= "000" & dac_reg;
						when 134 =>
							AD15_0out <= pwm1_t_on;
						when 135 =>
							AD15_0out <= pwm2_t_on;
						when 136 =>
							AD15_0out <= pwm3_t_on;
						when 160 =>
							AD15_0out <= ton_cnt;
						when 161 =>
							AD15_0out <= toff_cnt;
						when 197 =>
							AD15_0out <= "000000000000000" & led0_enable;
						when 198 =>
							AD15_0out <= test_reg;
						when 199 =>
							AD15_0out <= x"beef";
						when 200 =>
							AD15_0out <= "000000000000000" & dac_flag;
						when 201 =>
							AD15_0out <= "0000000000000" & dac_sela;
						when 202 =>
							AD15_0out <= "000000000000000" & dac_rng;
						when 203 =>
							AD15_0out <= "00000000" & dac_data;
						when 204 =>
							AD15_0out <= "000000000000000" & dac_load;
						when 205 =>
							AD15_0out <= "000000000000000" & dac_proc;
						when 206 =>
							AD15_0out <= "000" & dac_reg;
						when 256 to 287 =>
							AD15_0out <= "00000000" & ble_array(conv_integer(AD15_0(4 downto 0)));
						when 288 =>
							AD15_0out <= "000000000000000" & ble_go;
						when 289 =>
							AD15_0out <= "000000000000000" & ble_enable;
						when 290 =>
							AD15_0out <= "00000000" & ble_rst_reg;
						when 291 =>
							AD15_0out <= "000000000000000" & intr0_clear;
						when 1023 =>
							AD15_0out <= "000000000000000" & led_array_go;
						when 1024 to 1113 =>
							AD15_0out <= "00000000" & led_ram(conv_integer(radd(7 downto 0)));
						when 4096 to 4128 =>
							AD15_0out <= "00000000" & ble_array_rd(conv_integer(AD15_0(7 downto 0)));
						when others =>
							AD15_0out <= x"dead";
					end case;

					if (NOE = '0') then
						fsmc_bus_state <= reset;
					else
						fsmc_bus_state <= rd;
					end if;
			end case;
		end if;
	end process;

	--Memory Interface
	AD15_0 <= AD15_0out when (NE1 = '0' and NOE = '0') else (others => 'Z');

	--on board leds
	led0 <= (not led0_reg) and led0_enable;
	led1 <= not pwm1_reg;
	led2 <= not pwm2_reg;
	led3 <= not pwm3_reg;

	--BLE
	fpga_p1  <= ble_sck;                --sck
	ble_miso <= fpga_p3;                --miso
	fpga_p5  <= ble_wbyte(0);           --mosi;
	fpga_p7  <= ble_req;                --req
	ble_rdy  <= fpga_p9;                --rdy
	ble_act  <= fpga_p11;               --act
	fpga_p13 <= ble_rst_reg(0);         --rst;

	fpga_intr0 <= intr0_clear and intr0_en(0) and intr0;

	--Chris's board
	--fpga_p1 <= '0';
	--fan_in <= fpga_p3;
	--fpga_p5  <= not freq_gen1;
	--fpga_p9  <= dac_reg(12);
	--fpga_p11 <= dac_clk;
	--fpga_p13 <= dac_ldac;
	--fpga_p7  <= '0';
	--fpga_p15 <= dac_load;


	fpga_p0 <= dac_reg(12);             --led_array_dout; -- led array
	fpga_p2 <= dac_clk;                 --'0';
	fpga_p4 <= dac_ldac;
	fpga_p6 <= dac_load;

	fpga_p8  <= usecond;                --not freq_gen1;
	fan_in   <= fpga_p10;
	fpga_p15 <= led_array_dout;

end Behavioral;
